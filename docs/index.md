# Welcome to WDS Tripwire Wiki

Here you will find guides about Tripwire and wormholes in general.

## Pages

* If you're just starting out with Tripwire, visit [Tripwire Initial Setup](tripwire_setup/)
* A few useful tips and tricks about Tripwire can be found here: [Tripwire Tips and Tricks](tripwire_tips/)
* A 3rd party desktop app for finding shortest paths between systems, including wormholes: [Short Circuit](shortcircuit/)
* A guide to identifying the destination class of a wormhole based on its color: [Wormhole Visual Identification](whvisual/)
* A few fun facts about wormholes: [Fun Facts About Wormholes](funfacts/)

## External resources

* [Wormhole Hunting by Longinius Spear - Eve Vegas 2015](https://www.youtube.com/watch?v=5ddR7W4LmR8)
* [A history of Wormhole space in EVE Online by ExookiZ - Eve Fanfest 2016](https://www.youtube.com/watch?v=X9P0f3KJCtE)
* [Rykki's Guide - Sleeper Sites](https://docs.google.com/spreadsheets/d/17cNu8hxqJKqkkPnhDlIuJY-IT6ps7kTNCd3BEz0Bvqs/pubhtml#)

## Contributing

If you want to contribute to this wiki, please contact *Valtyr Farshield* on Slack. You can send pull requests here: <https://bitbucket.org/farshield/tripwire-wiki>
